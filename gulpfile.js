var gulp          = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    sass          = require('gulp-sass'),
    concat        = require('gulp-concat'),
    uglify        = require('gulp-uglify'),
    postcss = require('gulp-postcss'),
    rename        = require('gulp-rename'),
    notify        = require("gulp-notify"),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    browserSync   = require('browser-sync'),
    cache = require('gulp-cache');

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false,
        open: true,
        // online: false, // Work Offline Without Internet Connection
        // tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
    })
});

gulp.task('styles', function() {
    var plugins = [
        require('autoprefixer')({
            overrideBrowserslist: ['last 9 versions', '> 2%']
        }),
        require('postcss-pxtorem')({
            replace: true,
            propList: ['*'],
        }),
        require('cssnano')
    ];

    return gulp.src('app/scss/**/*.scss')
        .pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
        .pipe(postcss(plugins))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream())
});

gulp.task('code', function() {
    return gulp.src('app/*.html')
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('img', function () {
    return gulp.src('app/images/**/*.*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('app/images'))
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('watch', function() {
    gulp.watch('app/scss/**/*.scss', gulp.parallel('styles'));
    gulp.watch('app/*.html', gulp.parallel('code'))
});
gulp.task('default', gulp.parallel('watch', 'styles', 'browser-sync'));